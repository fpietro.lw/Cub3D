# Cub3D

## Description

This is a [42][1] school project inspired by the Wolfenstein 3D videogame. \
It is a simple raycasting engine that uses [miniLibX][2]. \
In order to run the game, the executable must be called from the command line, with a map file provided as argument: \
_ie_ \
`./cub3d maps/w3de1m1.cub`

## The map

In order for the program to accept a map file as valid, it must adhere to the following rules:
- the file format must be `.cub`
- the map can only include 6 characters:
	- **1** for walls
	- **0** for floor
	- **N**, **S**, **E** or **W** to denote the player's starting position and direction
- there can only be one player token in the map
- from the player's starting position, the map must be enclosed by walls
- the map layout must be at the end of the file, preceded by the following elements:
	- `NO ./path_to_the_north_texture`
	- `SO ./path_to_the_south_texture`
	- `WE ./path_to_the_west_texture`
	- `EA ./path_to_the_east_texture`
	- `F XXX,XXX,XXX` to define floor color
	- `C XXX,XXX,XXX` to define ceiling color
- the parameters for floor and celining color must be RGB colors in range 0,255

## Notes

To compile this program, the following packages need to be installed:
- xorg
- libx11-dev
- libxext-dev
- libbsd-dev 

On Debian based distros, just run the following command: \
`sudo apt update && sudo apt-get install xorg libx11-dev libxext-dev libbsd-dev`

This project was made in collaboration with [ddantas][3].


[1]: https://42lisboa.com/en
[2]: https://github.com/42Paris/minilibx-linux
[3]: github.com/ddantas42